package com.next.smartparkingticket.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.next.smartparkingticket.R;

public class ReportFragment extends Fragment implements MyFragment{

    View view;
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.report_fragment, container, false);
        recyclerView = view.findViewById(R.id.report_recyclerview);
//        BookingAdapter bookingAdapter = new BookingAdapter(mData);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        recyclerView.setAdapter(bookingAdapter);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mData = Sharing.Instance().getBookingViewModels();
    }

    @Override
    public void refresh() {
//        mData = Sharing.Instance().getBookingViewModels();
        if (getFragmentManager() != null) {
            getFragmentManager()
                    .beginTransaction()
                    .detach(ReportFragment.this)
                    .attach(ReportFragment.this)
                    .commit();
        }
    }
}
