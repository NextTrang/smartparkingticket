package com.next.smartparkingticket.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.next.smartparkingticket.R;
import com.next.smartparkingticket.databinding.CarInFragmentBinding;
import com.next.smartparkingticket.lib.Sharing;
import com.next.smartparkingticket.lib.car_in.CarInAdapter;
import com.next.smartparkingticket.lib.car_in.CarInEvent;
import com.next.smartparkingticket.lib.car_parking.CarParkingAdapter;
import com.next.smartparkingticket.lib.car_parking.CarParkingViewModel;

import java.util.ArrayList;
import java.util.List;

public class ParkingAreaFragment extends Fragment implements MyFragment{

    View view;
    private RecyclerView recyclerView;
    private List<CarParkingViewModel> mData = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.parking_area_fragment, container, false);

        recyclerView = view.findViewById(R.id.parking_area_recyclerview);
        CarParkingAdapter adapter = new CarParkingAdapter(this.getContext(),mData);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = Sharing.Instance().getCarParkingViewModels();
    }

    @Override
    public void refresh() {
        mData = Sharing.Instance().getCarParkingViewModels();
        if (getFragmentManager() != null) {
            getFragmentManager()
                    .beginTransaction()
                    .detach(ParkingAreaFragment.this)
                    .attach(ParkingAreaFragment.this)
                    .commit();
        }
    }
}