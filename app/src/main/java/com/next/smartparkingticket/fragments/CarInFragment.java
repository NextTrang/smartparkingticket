package com.next.smartparkingticket.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.next.smartparkingticket.R;
import com.next.smartparkingticket.databinding.CarInFragmentBinding;
import com.next.smartparkingticket.lib.Sharing;
import com.next.smartparkingticket.lib.car_in.CarInAdapter;
import com.next.smartparkingticket.lib.car_in.CarInEvent;
import com.next.smartparkingticket.lib.car_in.CarInViewModel;

import java.util.ArrayList;
import java.util.List;

public class CarInFragment extends Fragment implements MyFragment {

    View view;
    private RecyclerView recyclerView;
    private List<CarInViewModel> mData = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.car_in_fragment, container, false);
        CarInFragmentBinding binding = DataBindingUtil.bind(view);
        binding.setHandler(new CarInEvent(this.getContext()));

        recyclerView = view.findViewById(R.id.car_in_recyclerview);
        CarInAdapter carInAdapter = new CarInAdapter(this.getContext(),mData);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(carInAdapter);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = Sharing.Instance().getCarInViewModels();
    }

    @Override
    public void refresh() {
        mData = Sharing.Instance().getCarInViewModels();
        if (getFragmentManager() != null) {
            getFragmentManager()
                    .beginTransaction()
                    .detach(CarInFragment.this)
                    .attach(CarInFragment.this)
                    .commit();
        }
    }
}
