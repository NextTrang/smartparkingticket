package com.next.smartparkingticket.labels;

public class LbData {
    public static class LbInfo {
        public static final String CAR_NUMBER = "carNumber";
        public static final String CAR_CODE = "carCode";
        public static final String IMG_URL_RECOGNIZER ="imgUrlRecognizer";
        public static final String TIME_IN = "timeIn";
        public static final String PHONE_NUMBER = "phoneNumber";
        public static final String VEHICLE_TYPE = "vehicleType";
    }
}
