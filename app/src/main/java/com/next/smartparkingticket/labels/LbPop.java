package com.next.smartparkingticket.labels;

public class LbPop {

    public static class LbKey {
        public static final String POP_CODE = "PopCode";
        public static final String CAR_IN = "CarIn";
        public static final String CAR_PARKING = "CarParking";
        public static final String PHOTO_FILE = "PhotoFile";
        public static final String OPEN_CAMERA = "OpenCamera";
        public static final String TICKET = "Ticket";
        public static final String POP_CONFIRM = "PopConfirm";
        public static final String IMAGE_URL = "ImageUrl";
        public static final String CAR_CODE = "CarCode";
    }

    public static class LbCode {
        public static final int GET_NUMBER = 0;
        public static final int CAMERA_REQUEST = 1;
        public static final int CAR_IN_DETAIL = 2;
        public static final int CAR_PARKING_DETAIL = 3;
        public static final int QR_DISPLAY = 4;
        public static final int QR_SCANNER = 5;
        public static final int RECOGNIZER_DISPLAY = 6;
        public static final int REMOVE_TICKET = 7;
        public static final int CAR_PARKING_EDIT = 8;
    }

    public static class LbTitle {
        public static final String CHECK_CAR_OUT = "Mời xe ra";
    }

    public static class LbContent {
        public static final String CHECK_CAR_OUT = "Xe đã được đưa ra khỏi nhà xe";
    }
}
