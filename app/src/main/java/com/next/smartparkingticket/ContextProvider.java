package com.next.smartparkingticket;

import android.content.Context;

public interface ContextProvider {
    Context getActivityContext();
}
