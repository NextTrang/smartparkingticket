package com.next.smartparkingticket;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.next.smartparkingticket.data.DataLoadCarParking;
import com.next.smartparkingticket.fragments.CarInFragment;
import com.next.smartparkingticket.fragments.CarOutFragment;
import com.next.smartparkingticket.fragments.MyFragment;
import com.next.smartparkingticket.fragments.ParkingAreaFragment;
import com.next.smartparkingticket.fragments.ReportFragment;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.lib.ViewPagerAdapter;
import com.next.smartparkingticket.lib.car_parking.CarParkingEvent;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;

    private static int tabPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new DataLoadCarParking().execute(LbPop.LbKey.CAR_PARKING);

        setupUI();
        setOnClickTabs();
        setPermissions();
    }

    private void setupUI() {
        tabLayout = findViewById(R.id.tablayout_id);
        viewPager = findViewById(R.id.viewpager_id);

        adapter = new ViewPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        //Adding Fragment
        adapter.AddFragment(new CarInFragment(), "XE VÀO");
        adapter.AddFragment(new CarOutFragment(), "XE RA");
        adapter.AddFragment(new ParkingAreaFragment(), "NHÀ XE");
        adapter.AddFragment(new ReportFragment(), "BÁO CÁO");

        //adapter setup
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(tabPosition).select();

    }

    private void setOnClickTabs() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
                refresh();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    public void refresh() {
        Fragment fragment = adapter.getItem(tabPosition);
        ((MyFragment) fragment).refresh();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setPermissions() {
        requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            switch (requestCode) {
                case LbPop.LbCode.CAR_PARKING_DETAIL:
                    String code = data.getStringExtra(LbPop.LbKey.CAR_CODE);
                    int popSubCode = data.getIntExtra(LbPop.LbKey.POP_CODE, -1);

                    if(popSubCode == LbPop.LbCode.REMOVE_TICKET){
                        new CarParkingEvent(this).onRemove(code);
                    }
                    break;
            }
        }

    }
}
