package com.next.smartparkingticket.lib;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.next.smartparkingticket.lib.helper.Getter;
import com.next.smartparkingticket.lib.helper.OnSingleClickListener;

public class CustomBinding {

    @BindingAdapter({"android:QR"})
    public static void loadQR(ImageView view, String code) {
        Bitmap bitmap = Getter.getQRCode(view.getContext(), code);
        Glide.with(view.getContext())
                .load(bitmap)
                .into(view);
    }

    @BindingAdapter({"android:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl)
                .thumbnail(0.1f)
                .centerCrop()
                .into(view);
    }

    @BindingAdapter({"android:onSingleClick"})
    public static void onSingleClick(View view, OnSingleClickListener clickListener){
        view.setOnClickListener(clickListener);
    }
}
