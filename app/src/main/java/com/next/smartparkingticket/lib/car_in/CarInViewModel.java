package com.next.smartparkingticket.lib.car_in;

public class CarInViewModel {
    public String carNumber="";
    public String carCode="";

    private CarInNavigator navigator;

    public CarInViewModel(CarIn carIn) {
        this.carNumber = carIn.getCarNumber();
        this.carCode = carIn.getCarCode();
    }

    public void onItemClick(CarIn carIn) {
        navigator.onItemClick(carIn);
    }

    //Setter

    public void setNavigator(CarInNavigator navigator) {
        this.navigator = navigator;
    }
}
