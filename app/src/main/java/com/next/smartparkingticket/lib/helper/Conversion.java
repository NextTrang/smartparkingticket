package com.next.smartparkingticket.lib.helper;

import android.graphics.Bitmap;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Conversion {

    public static File Bitmap_2_ImageFile(Bitmap bitmap) {
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Next");
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }

        File image = null;
        try {
            image = new File(storageDir, "temp.jpg");
            OutputStream os = new BufferedOutputStream(new FileOutputStream(image));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.close();
        } catch (IOException e) {
            Log.d("MyLog", "CreatePhotoFile: " + e.getMessage());
        }

        return image;
    }

    public static long string_2_date_time_long(String strDate) throws ParseException {
        return new SimpleDateFormat("HH:mm dd/MM/yyyy").parse(strDate).getTime();
    }

    public static long string_2_date_long(String strDate) throws ParseException {
        return new SimpleDateFormat("dd/MM/yyyy").parse(strDate).getTime();
    }

    public static String date_time_2_string(long date) {
        return DateFormat.format("HH:mm dd/MM/yyyy", new Date(date)).toString();
    }

    public static String date_2_string(long date) {
        return DateFormat.format("dd/MM/yyyy", new Date(date)).toString();
    }
}
