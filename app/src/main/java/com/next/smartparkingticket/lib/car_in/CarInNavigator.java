package com.next.smartparkingticket.lib.car_in;

public interface CarInNavigator {
    void onItemClick(CarIn carIn);
}
