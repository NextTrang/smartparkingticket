package com.next.smartparkingticket.lib.car_parking;

public interface CarParkingNavigator {
    void onItemClick(CarParking carParking);
}
