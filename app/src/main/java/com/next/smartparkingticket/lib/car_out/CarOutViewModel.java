package com.next.smartparkingticket.lib.car_out;

public class CarOutViewModel {
    public String carNumber = "";
    public String timeIn ="";
    public String timeOut ="";

    public CarOutViewModel(CarOut carOut) {
        this.carNumber = carOut.getCarNumber();
        this.timeIn = carOut.getTimeIn();
        this.timeOut = carOut.getTimeOut();
    }
}
