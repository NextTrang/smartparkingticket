package com.next.smartparkingticket.lib.car_parking;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.next.smartparkingticket.databinding.ItemCarParkingBinding;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.pop_up.PopManagerActivity;

import java.util.List;

public class CarParkingAdapter extends RecyclerView.Adapter<CarParkingAdapter.MyViewHolder> implements CarParkingNavigator {

    private Context mContext;
    private List<CarParkingViewModel> mData;
    private LayoutInflater inflater;

    public CarParkingAdapter(Context mContext, List<CarParkingViewModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        ItemCarParkingBinding binding = ItemCarParkingBinding.inflate(inflater, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        mData.get(position).setNavigator(this);
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        }

        return mData.size();
    }

    @Override
    public void onItemClick(CarParking carParking) {
        Intent intent = new Intent(mContext, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.CAR_PARKING_DETAIL);
        intent.putExtra(LbPop.LbKey.CAR_PARKING, new CarParkingViewModel(carParking));
        ((AppCompatActivity) mContext).startActivityForResult(intent, LbPop.LbCode.CAR_PARKING_DETAIL);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ItemCarParkingBinding binding;

        public MyViewHolder(ItemCarParkingBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(CarParkingViewModel viewModel) {
            binding.setCarParkingViewModel(viewModel);
            binding.setCarParking(new CarParking(viewModel));
        }
    }
}

