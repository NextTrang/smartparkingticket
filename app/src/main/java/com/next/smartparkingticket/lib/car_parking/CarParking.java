package com.next.smartparkingticket.lib.car_parking;

import com.next.smartparkingticket.labels.LbData;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class CarParking implements Serializable {
    private String carNumber = "";
    private String carCode = "";
    private String timeIn ="";
    private String imgUrlRecognizer = "";
    private String vehicleType ="Xe số";
    private String price="";
    private String phoneNumber="";

    public CarParking() {
    }

    public CarParking(CarParkingViewModel viewModel) {
        this.carNumber = viewModel.carNumber;
        this.carCode = viewModel.carCode;
        this.timeIn = viewModel.timeIn;
        this.imgUrlRecognizer = viewModel.imgUrlRecognizer;
        this.vehicleType = viewModel.vehicleType;
        this.price = viewModel.price;
        this.phoneNumber = viewModel.phoneNumber;
    }

    public CarParking(JSONObject jsonInfo){
        try {
            carNumber = jsonInfo.getString(LbData.LbInfo.CAR_NUMBER);
            carCode = jsonInfo.getString(LbData.LbInfo.CAR_CODE);
            imgUrlRecognizer = jsonInfo.getString(LbData.LbInfo.IMG_URL_RECOGNIZER);
            timeIn = jsonInfo.getString(LbData.LbInfo.TIME_IN);
            phoneNumber = jsonInfo.getString(LbData.LbInfo.PHONE_NUMBER);
            vehicleType = jsonInfo.getString(LbData.LbInfo.VEHICLE_TYPE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Getter
    public String getCarNumber() {
        return carNumber;
    }

    public String getCarCode() {
        return carCode;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public String getImgUrlRecognizer() {
        return imgUrlRecognizer;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public String getPrice() {
        return price;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }


    //Setter

    public CarParking setCarNumber(String carNumber) {
        this.carNumber = carNumber;
        return this;
    }

    public CarParking setCarCode(String carCode) {
        this.carCode = carCode;
        return this;
    }

    public CarParking setTimeIn(String timeIn) {
        this.timeIn = timeIn;
        return this;
    }

    public CarParking setImgUrlRecognizer(String imgUrlRecognizer) {
        this.imgUrlRecognizer = imgUrlRecognizer;
        return this;
    }

    public CarParking setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }
}
