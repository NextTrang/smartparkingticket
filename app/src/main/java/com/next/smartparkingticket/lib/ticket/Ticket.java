package com.next.smartparkingticket.lib.ticket;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import java.io.Serializable;

public class Ticket extends BaseObservable implements Serializable, Comparable<Ticket> {
    private String code;
    private String imgUrl="";

    public Ticket(String code) {
        this.code = code;
    }

    //Getter

    public String getCode() {
        return code;
    }

    @Bindable
    public String getImgUrl() {
        return imgUrl;
    }

    //Setter

    public Ticket setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
        notifyPropertyChanged(com.next.smartparkingticket.BR.imgUrl);
        return this;
    }

    //Method
    @Override
    public int compareTo(Ticket o) {
        return this.code.compareTo(o.code);
    }
}
