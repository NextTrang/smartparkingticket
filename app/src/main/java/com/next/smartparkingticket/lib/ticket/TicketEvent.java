package com.next.smartparkingticket.lib.ticket;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.lib.Sharing;
import com.next.smartparkingticket.lib.car_in.CarIn;
import com.next.smartparkingticket.lib.car_in.CarInViewModel;
import com.next.smartparkingticket.lib.helper.Conversion;
import com.next.smartparkingticket.lib.helper.Devicer;
import com.next.smartparkingticket.lib.helper.Getter;
import com.next.smartparkingticket.pop_up.PopManagerActivity;

import java.io.File;
import java.net.URI;

public class TicketEvent {

    Context context;

    public TicketEvent(Context context) {
        this.context = context;
    }

    public void onClose(View view) {
        ((AppCompatActivity) context).finish();
    }

    public void onCreateNew(CarIn carIn, Ticket ticket) {
        carIn.setCarCode(ticket.getCode());

        for (CarInViewModel viewModel :
                Sharing.Instance().getCarInViewModels()) {
            if (viewModel.carNumber.equals(carIn.getCarNumber())) {
                viewModel.carCode = ticket.getCode();
                break;
            }
        }

        onShowQRDetail(ticket);
    }

    public void onShowQRDetail(Ticket ticket) {
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.QR_DISPLAY);
        intent.putExtra(LbPop.LbKey.TICKET, ticket);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.QR_DISPLAY);
    }

    public void onCaptureRecognizer(String carCode) {
        if(carCode.isEmpty()){
            Toast.makeText(context,"Bạn cần tạo vé trước khi chụp ảnh nhận diện",Toast.LENGTH_SHORT).show();
        }else {
            Devicer.openCamera(context, "");
        }
    }

    public void onShareTicket(Ticket ticket){
        Bitmap bitmap = Getter.getQRCode(context, ticket.getCode());
        File file = Conversion.Bitmap_2_ImageFile(bitmap);
        Uri path = FileProvider.getUriForFile(context,"com.next.smartparkingticket.fileprovider",file);

        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT,"Đây là vé xe của bạn");
        shareIntent.putExtra(Intent.EXTRA_STREAM,path);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.setType("image/*");
        context.startActivity(Intent.createChooser(shareIntent,"Share..."));
    }

    public void onRemoveTicketFromUsedList(String code){
        Ticket ticket = Sharing.Instance().getUsedTicket(code);
        Sharing.Instance().getTickets().add(ticket);
        java.util.Collections.sort(Sharing.Instance().getTickets());

        if (ticket != null) {
            Sharing.Instance().getUsedTickets().remove(ticket);
        }
    }
}
