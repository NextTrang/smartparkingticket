package com.next.smartparkingticket.lib.pop_confirm;

import java.io.Serializable;

public class PopConfirm implements Serializable {
    private String title="";
    private String content="";

    //Getter
    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    //Setter
    public PopConfirm setTitle(String title) {
        this.title = title;
        return this;
    }

    public PopConfirm setContent(String content) {
        this.content = content;
        return this;
    }
}
