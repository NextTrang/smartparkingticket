package com.next.smartparkingticket.lib.car_out;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.pop_up.PopManagerActivity;

public class CarOutEvent {

    Context context;

    public CarOutEvent(Context context) {
        this.context = context;
    }

    public void onOpenQRScanner(View view){
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.QR_SCANNER);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.QR_SCANNER);
    }
}
