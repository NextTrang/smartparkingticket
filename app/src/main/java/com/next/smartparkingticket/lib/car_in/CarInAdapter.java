package com.next.smartparkingticket.lib.car_in;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.next.smartparkingticket.databinding.ItemCarInBinding;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.pop_up.PopManagerActivity;

import java.util.List;

public class CarInAdapter extends RecyclerView.Adapter<CarInAdapter.MyViewHolder> implements CarInNavigator {

    private Context mContext;
    private List<CarInViewModel> mData;
    private LayoutInflater inflater;

    public CarInAdapter(Context mContext, List<CarInViewModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        ItemCarInBinding binding = ItemCarInBinding.inflate(inflater, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        mData.get(position).setNavigator(this);
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        }

        return mData.size();
    }

    @Override
    public void onItemClick(CarIn carIn) {
        Intent intent = new Intent(mContext, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.CAR_IN_DETAIL);
        intent.putExtra(LbPop.LbKey.CAR_IN, carIn);
        ((AppCompatActivity) mContext).startActivityForResult(intent, LbPop.LbCode.CAR_IN_DETAIL);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ItemCarInBinding binding;

        public MyViewHolder(ItemCarInBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(CarInViewModel viewModel) {
            binding.setCarInViewModel(viewModel);
            binding.setCarIn(new CarIn(viewModel));
        }
    }
}
