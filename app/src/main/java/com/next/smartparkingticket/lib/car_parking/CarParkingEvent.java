package com.next.smartparkingticket.lib.car_parking;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.next.smartparkingticket.data.DataSave;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.lib.Sharing;
import com.next.smartparkingticket.lib.ticket.TicketEvent;
import com.next.smartparkingticket.pop_up.PopManagerActivity;


public class CarParkingEvent {

    Context context;

    public CarParkingEvent(Context context) {
        this.context = context;
    }

    public void onClose(View view) {
        ((AppCompatActivity) context).finish();
    }

    public void onRemove(String code){

        //Remove from Car Parking list
        int index =-1;
        for (CarParkingViewModel viewModel : Sharing.Instance().getCarParkingViewModels()){
            if(viewModel.carCode.equals(code)){
                index = Sharing.Instance().getCarParkingViewModels().indexOf(viewModel);
            }
        }

        if(index != -1){
            Sharing.Instance().getCarParkingViewModels().remove(index);
        }

        //Remove ticket
        new TicketEvent(context).onRemoveTicketFromUsedList(code);

        //Save Data
        new DataSave().execute(LbPop.LbKey.CAR_PARKING,Sharing.Instance().getCarParkingViewModels());
    }

    public void onShowRemovePopup(String code){
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.REMOVE_TICKET);
        intent.putExtra(LbPop.LbKey.CAR_CODE, code);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.REMOVE_TICKET);
    }

    public void onShowEditPopup(CarParkingViewModel carParkingViewModel){
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.CAR_PARKING_EDIT);
        intent.putExtra(LbPop.LbKey.CAR_PARKING, carParkingViewModel);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.CAR_PARKING_EDIT);
    }

    public void onTransferData(CarParkingViewModel carParkingViewModel){
        Intent i = new Intent();
        i.putExtra(LbPop.LbKey.CAR_PARKING, carParkingViewModel);
        AppCompatActivity activity = ((AppCompatActivity) context);
        activity.setResult(activity.RESULT_OK, i);
        activity.finish();
    }

    public void onUpdateInfo(CarParkingViewModel carParkingViewModel){
        for (CarParkingViewModel viewModel : Sharing.Instance().getCarParkingViewModels()){
            if(viewModel.carCode.equals(carParkingViewModel.carCode)){
                viewModel.price = carParkingViewModel.price;
                viewModel.phoneNumber = carParkingViewModel.phoneNumber;
                viewModel.vehicleType = carParkingViewModel.vehicleType;
                viewModel.timeIn = carParkingViewModel.timeIn;
                break;
            }
        }

        new DataSave().execute(LbPop.LbKey.CAR_PARKING,Sharing.Instance().getCarParkingViewModels());
    }

    public void onSetVehicleType(CarParkingViewModel carParkingViewModel,String type){
        carParkingViewModel.vehicleType = type;
    }
}
