package com.next.smartparkingticket.lib;

import com.next.smartparkingticket.lib.car_in.CarInViewModel;
import com.next.smartparkingticket.lib.car_out.CarOutViewModel;
import com.next.smartparkingticket.lib.car_parking.CarParkingViewModel;
import com.next.smartparkingticket.lib.helper.Getter;
import com.next.smartparkingticket.lib.ticket.Ticket;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Sharing {
    private static Sharing _sharing = null;

    public static Sharing Instance() {
        if (_sharing == null) {
            _sharing = new Sharing();
        }

        return _sharing;
    }

    private List<CarInViewModel> carInViewModels = new ArrayList<>();
    private List<CarParkingViewModel> carParkingViewModels = new ArrayList<>();
    private List<CarOutViewModel> carOutViewModels = new ArrayList<>();
    private List<Ticket> tickets = new ArrayList<>();
    private List<Ticket> usedTickets = new ArrayList<>();

    //Getter

    public List<CarInViewModel> getCarInViewModels() {
        return carInViewModels;
    }

    public List<CarParkingViewModel> getCarParkingViewModels() {
        return carParkingViewModels;
    }

    public List<CarOutViewModel> getCarOutViewModels() {
        return carOutViewModels;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public Ticket getTicket(){
        if(tickets.isEmpty()){
            tickets.addAll(Getter.getNewTickets());
        }

        Random random = new Random();
        int randIndex = random.nextInt(tickets.size());

        Ticket ticket = tickets.get(randIndex);
        usedTickets.add(ticket);
        tickets.remove(randIndex);

        return ticket;
    }

    public List<Ticket> getUsedTickets() {
        return usedTickets;
    }

    public Ticket getUsedTicket(String code){
        for (Ticket ticket:usedTickets){
            if (ticket.getCode().equals(code)){
                return ticket;
            }
        }

        return null;
    }

    //Method
    public void reset() {
        carInViewModels.clear();
        carParkingViewModels.clear();
        tickets.clear();
        usedTickets.clear();
        tickets.clear();
    }
}
