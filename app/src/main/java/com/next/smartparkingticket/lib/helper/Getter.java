package com.next.smartparkingticket.lib.helper;

import android.app.Application;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.google.zxing.WriterException;
import com.next.smartparkingticket.App;
import com.next.smartparkingticket.lib.Sharing;
import com.next.smartparkingticket.lib.ticket.Ticket;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class Getter {

    private static final String TAG = "Getter";

    public static List<Ticket> getNewTickets() {
        List<Ticket> result = new ArrayList<>();

        //get range of current ticket list
        int currentMax = -1;
        int size = Sharing.Instance().getTickets().size() - 1;
        if (!Sharing.Instance().getTickets().isEmpty() && size > 1) {
            currentMax = Integer.parseInt(Sharing.Instance().getTickets().get(size - 1).getCode());
        }

        int min, max;
        if (currentMax == -1) {
            min = 100;
            max = 1000;
        } else {
            min = currentMax + 1;
            max = min + 1000;
        }

        for (int i = min; i < max + 1; i++) {
            boolean used = false;
            for (Ticket usedTicket : Sharing.Instance().getUsedTickets()) {
                if (usedTicket.getCode().equals(i)) {
                    used = true;
                    Log.d(TAG, "getNewTickets: this ticket was used - " + i);
                    break;
                }
            }

            if (used == false) {
                result.add(new Ticket(String.valueOf(i)));
            }
        }

        return result;

    }

    public static Bitmap getQRCode(Context context, String input) {
        Bitmap result = null;

        if (input.length() > 0) {
            WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;
            QRGEncoder qrgEncoder = new QRGEncoder(input, null, QRGContents.Type.TEXT, smallerDimension);
            try {
                result = qrgEncoder.encodeAsBitmap();
            } catch (WriterException e) {
                Log.e(TAG, "getQRCode: ", e);
            }
        }

        return result;
    }

    public static String getRealPathFromURI(Context context, Uri uri) {
        if (uri.getScheme().equals("file")) {
            return uri.getPath();
        }

        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();
        return path;
    }
}
