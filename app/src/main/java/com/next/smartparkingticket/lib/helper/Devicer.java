package com.next.smartparkingticket.lib.helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.next.smartparkingticket.labels.LbPop;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Devicer {
    public static File photoFile = null;

    public static void openCamera(Context context, String file_name) {
        Intent takePic = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePic.resolveActivity(context.getPackageManager()) != null) {
            photoFile = null;
            try {
                photoFile = createPhotoFile(file_name);

                if (photoFile != null) {
                    Uri photoUri = FileProvider.getUriForFile(context, "com.next.smartparkingticket.fileprovider", photoFile);
                    takePic.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                    ((AppCompatActivity) context).startActivityForResult(takePic, LbPop.LbCode.CAMERA_REQUEST);
                }
            } catch (Exception e) {
                Log.d("MyLog", "dispatchPictureTakerAction: " + e.getMessage());
            }
        }
    }

    private static File createPhotoFile(String file_name) {

        String name = (file_name.isEmpty()) ? new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) : file_name;

        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Next");
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }

        File image = null;
        try {
            image = new File(storageDir, name + ".jpg");
            if(!image.exists()){
                image.createNewFile();
            }
        } catch (IOException e) {
            Log.d("MyLog", "CreatePhotoFile: " + e.getMessage());
        }

        return image;
    }

}
