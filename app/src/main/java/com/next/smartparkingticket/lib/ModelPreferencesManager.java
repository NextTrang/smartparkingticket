package com.next.smartparkingticket.lib;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class ModelPreferencesManager {
    private static ModelPreferencesManager _model = null;

    public static ModelPreferencesManager Instance() {
        if (_model == null) {
            _model = new ModelPreferencesManager();
        }

        return _model;
    }

    private SharedPreferences preferences;
    private static final String PREFERENCES_FILE_NAME   = "DATA";

    public void with(Application application){
        preferences = application.getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void push(String key,Object object){
        //Convert object to JSON String.
        Gson gson = new Gson();
        String jsonString = gson.toJson(object);
        //Save that String in SharedPreferences
        preferences.edit().putString(key,jsonString).apply();
    }

    public String get(String key){
        String value = preferences.getString(key, null);
        return value;
    }
}
