package com.next.smartparkingticket.lib;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.next.smartparkingticket.BR;

import java.io.Serializable;

public class Car extends BaseObservable implements Serializable {
    private String carNumber="";

    //Getter
    @Bindable
    public String getCarNumber() {
        return carNumber;
    }

    //Setter

    public Car setCarNumber(String carNumber) {
        this.carNumber = carNumber;
        notifyPropertyChanged(com.next.smartparkingticket.BR.carNumber);
        return this;
    }
}
