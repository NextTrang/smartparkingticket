package com.next.smartparkingticket.lib.car_in;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.next.smartparkingticket.BR;

import java.io.Serializable;

public class CarIn extends BaseObservable implements Serializable {
    private String carNumber = "";
    private String carCode = "";

    public CarIn() {
    }

    public CarIn(CarInViewModel viewModel) {
        this.carNumber = viewModel.carNumber;
        this.carCode = viewModel.carCode;
    }

//Getter

    public String getCarNumber() {
        return carNumber;
    }

    @Bindable
    public String getCarCode() {
        return carCode;
    }

    //Setter

    public CarIn setCarNumber(String carNumber) {
        this.carNumber = carNumber;
        return this;
    }

    public CarIn setCarCode(String carCode) {
        this.carCode = carCode;
        notifyPropertyChanged(com.next.smartparkingticket.BR.carCode);
        return this;
    }
}
