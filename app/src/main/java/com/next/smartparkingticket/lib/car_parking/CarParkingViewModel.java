package com.next.smartparkingticket.lib.car_parking;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import java.io.Serializable;

public class CarParkingViewModel extends BaseObservable implements Serializable {
    public String carNumber = "";
    public String carCode = "";
    @Bindable public String timeIn = "";
    public String imgUrlRecognizer = "";
    @Bindable public String vehicleType = "Xe số";
    public String price = "";
    @Bindable public String phoneNumber="";

    private transient CarParkingNavigator navigator;

    public CarParkingViewModel(CarParking carParking) {
        this.carNumber = carParking.getCarNumber();
        this.carCode = carParking.getCarCode();
        this.timeIn = carParking.getTimeIn();
        this.imgUrlRecognizer = carParking.getImgUrlRecognizer();
        this.vehicleType = carParking.getVehicleType();
        this.price = carParking.getPrice();
        this.phoneNumber = carParking.getPhoneNumber();
    }

    public void onItemClick(CarParking carParking) {
        navigator.onItemClick(carParking);
    }

    public void setNavigator(CarParkingNavigator navigator) {
        this.navigator = navigator;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
        notifyPropertyChanged(com.next.smartparkingticket.BR.timeIn);
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        notifyPropertyChanged(com.next.smartparkingticket.BR.phoneNumber);
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
        notifyPropertyChanged(com.next.smartparkingticket.BR.vehicleType);
    }
}
