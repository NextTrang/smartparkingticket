package com.next.smartparkingticket.lib.helper;

import com.next.smartparkingticket.lib.Sharing;
import com.next.smartparkingticket.lib.car_in.CarInViewModel;
import com.next.smartparkingticket.lib.car_parking.CarParkingViewModel;

import java.util.List;

public class Checker {

    public static boolean existInWaitingList(String carNumber){
        for (CarInViewModel viewModel: Sharing.Instance().getCarInViewModels()){
            if(viewModel.carNumber.equals(carNumber.toUpperCase())){
                return true;
            }
        }

        return false;
    }

    public static boolean existInParkingList(String carNumber){
        for(CarParkingViewModel viewModel:Sharing.Instance().getCarParkingViewModels()){
            if(viewModel.carNumber.equals(carNumber.toUpperCase())){
                return true;
            }
        }

        return false;
    }
}
