package com.next.smartparkingticket.lib.car_out;

public class CarOut {
    private String carNumber = "";
    private String timeIn ="";
    private String timeOut ="";

    //Getter

    public String getCarNumber() {
        return carNumber;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public String getTimeOut() {
        return timeOut;
    }

    //Setter

    public CarOut setCarNumber(String carNumber) {
        this.carNumber = carNumber;
        return this;
    }

    public CarOut setTimeIn(String timeIn) {
        this.timeIn = timeIn;
        return this;
    }

    public CarOut setTimeOut(String timeOut) {
        this.timeOut = timeOut;
        return this;
    }
}
