package com.next.smartparkingticket.lib;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.next.smartparkingticket.R;

import java.util.ArrayList;
import java.util.Calendar;

public class TimeItemRecycleViewAdapter extends RecyclerView.Adapter<TimeItemRecycleViewAdapter.ViewHolder> {
    private ArrayList<String> mItemList;
    private Context mContext;
    private int checkedPosition = 0;
    private OnSelectTimeInteractionListener onClickListener;
    private int hour_or_min = 0;

    public TimeItemRecycleViewAdapter(Context context, ArrayList<String> itemList, OnSelectTimeInteractionListener listener, int hour_or_min) {
        mContext = context;
        mItemList = itemList;
        onClickListener = listener;
        this.hour_or_min = hour_or_min;
    }

    public void setTimeTable(ArrayList<String> timeTable) {
        mItemList = new ArrayList<>();
        mItemList = timeTable;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_time, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mItemList.get(position));
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            textView = itemView.findViewById(R.id.time);
        }

        public void bind(final String timeTag) {
            if (checkedPosition == -1) {
                mView.setSelected(false);
            } else {
                if (checkedPosition == getAdapterPosition()) {
                    mView.setSelected(true);
                } else {
                    mView.setSelected(false);
                }
            }

            textView.setText(timeTag);
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mView.setSelected(true);
                    if (checkedPosition != getAdapterPosition()) {
                        notifyItemChanged(checkedPosition);
                        checkedPosition = getAdapterPosition();
                        if (hour_or_min == 0) {
                            onClickListener.onSelectHourInteraction(timeTag);
                        } else {
                            onClickListener.onSelectMinInteraction(timeTag);
                        }
                    }
                }
            });
        }
    }

    public void setSelectTime(int position) {
        checkedPosition = position;
        notifyItemChanged(checkedPosition);
    }

    public String getSelected() {
        if (checkedPosition != -1) {
            return mItemList.get(checkedPosition);
        }

        return null;
    }

    public int getPositionFromDate(Calendar date) {
        int hour = date.get(Calendar.HOUR_OF_DAY);
        int min = date.get(Calendar.MINUTE);

        int idx = hour * 2;
        if (min >= 30) {
            idx += 1;
        }

        return idx;
    }

    public interface OnSelectTimeInteractionListener {
        void onSelectHourInteraction(String item);

        void onSelectMinInteraction(String item);
    }
}

