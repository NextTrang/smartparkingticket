package com.next.smartparkingticket.lib.car_in;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.next.smartparkingticket.data.DataSave;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.lib.Car;
import com.next.smartparkingticket.lib.Sharing;
import com.next.smartparkingticket.lib.car_parking.CarParking;
import com.next.smartparkingticket.lib.car_parking.CarParkingViewModel;
import com.next.smartparkingticket.lib.helper.Checker;
import com.next.smartparkingticket.lib.ticket.Ticket;
import com.next.smartparkingticket.pop_up.PopManagerActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CarInEvent {

    Context context;

    public CarInEvent(Context context) {
        this.context = context;
    }

    public void onClose(View view) {
        ((AppCompatActivity) context).finish();
    }

    public void onTapGetNumber(View view) {
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.GET_NUMBER);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.GET_NUMBER);
    }

    public void onAddToWaitingList(Car car) {
        if (car.getCarNumber().isEmpty()) {
            Toast.makeText(context, "Bạn chưa nhập biển số xe", Toast.LENGTH_SHORT).show();
            return;
        } else if (Checker.existInWaitingList(car.getCarNumber())) {
            Toast.makeText(context, "Số này đã có trong danh sách chờ", Toast.LENGTH_SHORT).show();
            return;
        }else if (Checker.existInParkingList(car.getCarNumber())){
            Toast.makeText(context, "Số này đã có trong nhà xe", Toast.LENGTH_SHORT).show();
            return;
        }

        CarIn carIn = new CarIn().setCarNumber(car.getCarNumber().toUpperCase());
        Sharing.Instance().getCarInViewModels().add(new CarInViewModel(carIn));

        Toast.makeText(context, carIn.getCarNumber().replaceAll("\n", " ") + " đã được thêm vào danh sách chờ", Toast.LENGTH_SHORT).show();
    }

    public void onRemoveCarIn(CarInViewModel carInViewModel) {
        int index = -1;
        for (CarInViewModel viewModel : Sharing.Instance().getCarInViewModels()) {
            if (viewModel.carNumber.equals(carInViewModel.carNumber)) {
                index = Sharing.Instance().getCarInViewModels().indexOf(viewModel);
                break;
            }
        }

        if (index != -1) {
            Sharing.Instance().getCarInViewModels().remove(index);
        }

        ((AppCompatActivity) context).finish();
    }

    public void onPark(CarInViewModel viewModel,Ticket ticket) {
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
        String timeIn = dateFormat.format(currentTime);

        int index = -1;
        for (CarInViewModel carInViewModel : Sharing.Instance().getCarInViewModels()) {
            if (carInViewModel.carNumber.equals(viewModel.carNumber)) {
                CarParking carParking = new CarParking().setCarCode(carInViewModel.carCode)
                        .setCarNumber(carInViewModel.carNumber)
                        .setTimeIn(timeIn)
                        .setImgUrlRecognizer(ticket.getImgUrl());

                Sharing.Instance().getCarParkingViewModels().add(new CarParkingViewModel(carParking));
                //save data
                new DataSave().execute(LbPop.LbKey.CAR_PARKING,Sharing.Instance().getCarParkingViewModels());

                index = Sharing.Instance().getCarInViewModels().indexOf(carInViewModel);
                break;
            }
        }

        if(index != -1){
            Sharing.Instance().getCarInViewModels().remove(index);
        }

        ((AppCompatActivity) context).finish();
    }
}
