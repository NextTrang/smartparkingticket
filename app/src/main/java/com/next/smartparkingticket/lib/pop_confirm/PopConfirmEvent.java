package com.next.smartparkingticket.lib.pop_confirm;

import android.content.Context;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class PopConfirmEvent {
    Context context;

    public PopConfirmEvent(Context context) {
        this.context = context;
    }

    public void onClose(View view) {
        ((AppCompatActivity) context).finish();
    }
}
