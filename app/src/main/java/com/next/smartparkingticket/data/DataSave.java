package com.next.smartparkingticket.data;

import com.next.smartparkingticket.lib.ModelPreferencesManager;

public class DataSave {
    public void execute(String key,Object object) {
        ModelPreferencesManager.Instance().push(key,object);
    }
}
