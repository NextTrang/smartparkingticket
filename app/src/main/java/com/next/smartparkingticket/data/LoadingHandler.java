package com.next.smartparkingticket.data;

public interface LoadingHandler {
    void execute(String key);
}
