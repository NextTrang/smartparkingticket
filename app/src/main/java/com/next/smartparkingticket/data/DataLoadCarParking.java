package com.next.smartparkingticket.data;

import android.util.Log;

import com.next.smartparkingticket.lib.ModelPreferencesManager;
import com.next.smartparkingticket.lib.Sharing;
import com.next.smartparkingticket.lib.car_parking.CarParking;
import com.next.smartparkingticket.lib.car_parking.CarParkingViewModel;
import com.next.smartparkingticket.lib.ticket.Ticket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DataLoadCarParking implements LoadingHandler {

    @Override
    public void execute(String key) {
        String data = ModelPreferencesManager.Instance().get(key);
        if(data == null){
            //no data -> stop loading
            return;
        }

        Log.d("loadData", data);

        try {
            JSONArray jsonInfos = new JSONArray(data);

            for (int i = 0; i < jsonInfos.length(); ++i) {
                JSONObject jsonInfo = jsonInfos.getJSONObject(i);

                CarParking carParking = new CarParking(jsonInfo);
                Sharing.Instance().getCarParkingViewModels().add(new CarParkingViewModel(carParking));

                Ticket usedTicket = new Ticket(carParking.getCarCode())
                        .setImgUrl(carParking.getImgUrlRecognizer());
                Sharing.Instance().getUsedTickets().add(usedTicket);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
