package com.next.smartparkingticket.pop_up;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.next.smartparkingticket.R;
import com.next.smartparkingticket.databinding.PopQRDisplayBinding;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.lib.car_in.CarIn;
import com.next.smartparkingticket.lib.ticket.Ticket;
import com.next.smartparkingticket.lib.ticket.TicketEvent;

public class PopQRDisplayActivity extends AppCompatActivity {

    private Ticket data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PopQRDisplayBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_qrdisplay);

        data = (Ticket) getIntent().getSerializableExtra(LbPop.LbKey.TICKET);
        binding.setTicket(data);
        binding.setHandler(new TicketEvent(this));
    }
}
