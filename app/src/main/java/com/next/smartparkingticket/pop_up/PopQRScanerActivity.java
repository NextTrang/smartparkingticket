package com.next.smartparkingticket.pop_up;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.zxing.Result;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.next.smartparkingticket.data.DataSave;
import com.next.smartparkingticket.R;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.lib.Sharing;
import com.next.smartparkingticket.lib.car_out.CarOut;
import com.next.smartparkingticket.lib.car_out.CarOutViewModel;
import com.next.smartparkingticket.lib.car_parking.CarParkingViewModel;
import com.next.smartparkingticket.lib.pop_confirm.PopConfirm;
import com.next.smartparkingticket.lib.ticket.TicketEvent;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class PopQRScanerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView scannerView;
    private TextView txtResult;
    private ImageButton ibtnRescan;
    private ImageButton ibtnClose;
    private ImageButton ibtnCarOut;
    private ImageView imgRecognizer;

    private int detectedIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_qrscaner);

        //binding
        setBinding();

        //Request Permission
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        scannerView.setResultHandler(PopQRScanerActivity.this);
                        scannerView.startCamera();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        Toast.makeText(PopQRScanerActivity.this, "You must accept this permission", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                    }
                }).check();

        setEvents();
    }

    private void setBinding() {
        scannerView = findViewById(R.id.qrScanner);
        txtResult = findViewById(R.id.tvResult);
        ibtnRescan = findViewById(R.id.ibtnRescan);
        ibtnClose = findViewById(R.id.ibtnClose);
        ibtnCarOut = findViewById(R.id.ibtnCarOut);
        imgRecognizer = findViewById(R.id.imgRecognizer);
    }

    private void setEvents() {
        ibtnRescan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtResult.setText("...");
                imgRecognizer.setImageResource(R.drawable.ic_person_grey_24dp);
                scannerView.setResultHandler(PopQRScanerActivity.this);
                scannerView.startCamera();
            }
        });

        ibtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ibtnCarOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detectedIndex != -1) {
                    CarParkingViewModel carParkingViewModel = Sharing.Instance().getCarParkingViewModels().get(detectedIndex);

                    //Move to CarOut list
                    CarOut carOut = new CarOut().setCarNumber(carParkingViewModel.carNumber);
                    Sharing.Instance().getCarOutViewModels().add(new CarOutViewModel(carOut));

                    //Update ticket list
                    new TicketEvent(PopQRScanerActivity.this).onRemoveTicketFromUsedList(carParkingViewModel.carCode);

                    //Remove from CarParking list
                    Sharing.Instance().getCarParkingViewModels().remove(detectedIndex);
                    //SaveData
                    new DataSave().execute(LbPop.LbKey.CAR_PARKING,Sharing.Instance().getCarParkingViewModels());

                    detectedIndex = -1;

                    PopConfirm popConfirm = new PopConfirm()
                            .setTitle(LbPop.LbTitle.CHECK_CAR_OUT)
                            .setContent(LbPop.LbContent.CHECK_CAR_OUT);
                    Intent intent = new Intent(PopQRScanerActivity.this, PopConfirmActivity.class);
                    intent.putExtra(LbPop.LbKey.POP_CONFIRM, popConfirm);
                    startActivity(intent);

                    //Prepare for new scan
                    txtResult.setText("...");
                    imgRecognizer.setImageResource(R.drawable.ic_person_grey_24dp);
                }
            }
        });

        imgRecognizer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detectedIndex != -1) {
                    CarParkingViewModel carParkingViewModel = Sharing.Instance().getCarParkingViewModels().get(detectedIndex);

                    Intent intent = new Intent(PopQRScanerActivity.this, PopManagerActivity.class);
                    intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.RECOGNIZER_DISPLAY);
                    intent.putExtra(LbPop.LbKey.IMAGE_URL, carParkingViewModel.imgUrlRecognizer);
                    startActivityForResult(intent, LbPop.LbCode.RECOGNIZER_DISPLAY);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        scannerView.stopCamera();
        super.onDestroy();
    }

    @Override
    public void handleResult(Result rawResult) {
        String ticketCode = rawResult.getText();
        String output = "Không tìm thấy mã vé";
        detectedIndex = -1;
        for (CarParkingViewModel viewModel : Sharing.Instance().getCarParkingViewModels()) {
            if (viewModel.carCode.equals(ticketCode)) {
                output = viewModel.carNumber;
                detectedIndex = Sharing.Instance().getCarParkingViewModels().indexOf(viewModel);
                Glide.with(this)
                        .load(viewModel.imgUrlRecognizer)
                        .thumbnail(0.1f)
                        .centerCrop()
                        .into(imgRecognizer);
                break;
            }
        }

        txtResult.setText(output);
    }
}
