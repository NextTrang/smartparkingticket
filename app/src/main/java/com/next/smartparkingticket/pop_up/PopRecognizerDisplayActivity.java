package com.next.smartparkingticket.pop_up;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.WindowManager;

import com.next.smartparkingticket.R;
import com.next.smartparkingticket.databinding.PopRecognizerDisplayBinding;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.lib.ticket.Ticket;

public class PopRecognizerDisplayActivity extends AppCompatActivity {

    private String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PopRecognizerDisplayBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_recognizer_display);

        data = getIntent().getStringExtra(LbPop.LbKey.IMAGE_URL);
        binding.setImgUrl(data);

        setupPopup();
    }

    private void setupPopup() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * .8));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = 0;
        getWindow().setAttributes(params);
    }
}
