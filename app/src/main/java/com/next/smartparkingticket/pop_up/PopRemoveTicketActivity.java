package com.next.smartparkingticket.pop_up;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

import com.next.smartparkingticket.R;
import com.next.smartparkingticket.labels.LbPop;

public class PopRemoveTicketActivity extends AppCompatActivity {

    private Button btnYes;
    private Button btnNo;
    private ImageButton ibtnClose;

    private Intent data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_remove_ticket);

        data = getIntent();

        setupPopup();
        setupButtons();
    }

    private void setupPopup() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .9), (int) (height * .5));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = 0;
        getWindow().setAttributes(params);
    }

    private void setupButtons() {
        //binding buttons
        btnNo = findViewById(R.id.btnNo);
        btnYes = findViewById(R.id.btnYes);
        ibtnClose = findViewById(R.id.ibtnClose);

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK, data);
                finish();
            }
        });

        ibtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
