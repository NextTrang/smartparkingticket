package com.next.smartparkingticket.pop_up;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;

import com.next.smartparkingticket.R;
import com.next.smartparkingticket.databinding.PopCarParkingDetailBinding;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.lib.Sharing;
import com.next.smartparkingticket.lib.car_parking.CarParking;
import com.next.smartparkingticket.lib.car_parking.CarParkingEvent;
import com.next.smartparkingticket.lib.car_parking.CarParkingViewModel;
import com.next.smartparkingticket.lib.ticket.TicketEvent;

public class PopCarParkingDetailActivity extends AppCompatActivity {

    private CarParkingViewModel mData;
    private CarParkingEvent handler = new CarParkingEvent(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PopCarParkingDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_car_parking_detail);

        //binding
        mData = (CarParkingViewModel) getIntent().getSerializableExtra(LbPop.LbKey.CAR_PARKING);
        binding.setCarParkingViewModel(mData);
        binding.setHandler(handler);
        binding.setTicket(Sharing.Instance().getUsedTicket(mData.carCode));
        binding.setTicketHandler(new TicketEvent(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            switch (requestCode) {
                case LbPop.LbCode.REMOVE_TICKET:
                    data.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.REMOVE_TICKET);
                    setResult(RESULT_OK, data);
                    finish();
                    break;
                case LbPop.LbCode.CAR_PARKING_EDIT:
                    CarParkingViewModel carParkingViewModel = (CarParkingViewModel) data.getSerializableExtra(LbPop.LbKey.CAR_PARKING);

                    //update mData for UI
                    mData.setTimeIn(carParkingViewModel.timeIn);
                    mData.setPhoneNumber(carParkingViewModel.phoneNumber);
                    mData.setVehicleType(carParkingViewModel.vehicleType);

                    handler.onUpdateInfo(carParkingViewModel);
                    break;
                default:
                    setResult(RESULT_OK, data);
                    finish();
            }
        }
    }
}
