package com.next.smartparkingticket.pop_up;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.os.Handler;

import com.next.smartparkingticket.R;
import com.next.smartparkingticket.databinding.PopComfirmBinding;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.lib.pop_confirm.PopConfirm;
import com.next.smartparkingticket.lib.pop_confirm.PopConfirmEvent;

public class PopConfirmActivity extends AppCompatActivity {

    private PopConfirm popConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PopComfirmBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_pop_confirm);

        popConfirm = (PopConfirm) getIntent().getSerializableExtra(LbPop.LbKey.POP_CONFIRM);
        binding.setPopConfirm(popConfirm);
        binding.setHandler(new PopConfirmEvent(this));

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                finish();
            }
        }, 2000);
    }
}
