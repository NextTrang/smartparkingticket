package com.next.smartparkingticket.pop_up;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.next.smartparkingticket.R;
import com.next.smartparkingticket.databinding.PopCarParkingDetailBinding;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.lib.car_parking.CarParkingEvent;

public class PopManagerActivity extends AppCompatActivity {

    private int popCode = -1;
    private Intent mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_manager);

        mData = getIntent();
        popCode = mData.getIntExtra(LbPop.LbKey.POP_CODE, -1);
        startPopup(popCode);
    }

    public void startPopup(int popCode) {
        switch (popCode) {
            case LbPop.LbCode.GET_NUMBER:
                Intent iGetNumber = new Intent(this, PopGetNumberActivity.class);
                startActivityForResult(iGetNumber, popCode);
                break;
            case LbPop.LbCode.CAR_IN_DETAIL:
                Intent iCarInDetail = new Intent(this, PopCarInDetailActivity.class);
                iCarInDetail.putExtras(mData);
                startActivityForResult(iCarInDetail, popCode);
                break;
            case LbPop.LbCode.CAR_PARKING_DETAIL:
                Intent iCarParkingDetail = new Intent(this, PopCarParkingDetailActivity.class);
                iCarParkingDetail.putExtras(mData);
                startActivityForResult(iCarParkingDetail, popCode);
                break;
            case LbPop.LbCode.QR_DISPLAY:
                Intent iQRDisplay = new Intent(this, PopQRDisplayActivity.class);
                iQRDisplay.putExtras(mData);
                startActivityForResult(iQRDisplay, popCode);
                break;
            case LbPop.LbCode.QR_SCANNER:
                Intent iQRScanner = new Intent(this, PopQRScanerActivity.class);
                startActivityForResult(iQRScanner, popCode);
                break;
            case LbPop.LbCode.RECOGNIZER_DISPLAY:
                Intent iRecognizerDisplay = new Intent(this, PopRecognizerDisplayActivity.class);
                iRecognizerDisplay.putExtras(mData);
                startActivityForResult(iRecognizerDisplay, popCode);
                break;
            case LbPop.LbCode.REMOVE_TICKET:
                Intent iDeletedTicket = new Intent(this, PopRemoveTicketActivity.class);
                iDeletedTicket.putExtras(mData);
                startActivityForResult(iDeletedTicket, popCode);
                break;
            case LbPop.LbCode.CAR_PARKING_EDIT:
                Intent iCarParkingEdit = new Intent(this, PopEditCarParkingActivity.class);
                iCarParkingEdit.putExtras(mData);
                startActivityForResult(iCarParkingEdit, popCode);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Toast.makeText(this, "Result code: " + resultCode + " - RequestCode: " + requestCode, Toast.LENGTH_SHORT).show();

        if (data != null) {
            switch (requestCode) {
                default:
                    setResult(RESULT_OK, data);
            }
        }

        finish();
    }
}
