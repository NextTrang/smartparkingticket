package com.next.smartparkingticket.pop_up;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.haibin.calendarview.CalendarView;
import com.next.smartparkingticket.R;
import com.next.smartparkingticket.databinding.PopEditCarParkingBinding;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.lib.TimeItemRecycleViewAdapter;
import com.next.smartparkingticket.lib.car_parking.CarParkingEvent;
import com.next.smartparkingticket.lib.car_parking.CarParkingViewModel;
import com.next.smartparkingticket.lib.helper.Conversion;
import com.next.smartparkingticket.lib.helper.SimpleWeekView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class PopEditCarParkingActivity extends AppCompatActivity implements CalendarView.OnCalendarSelectListener, TimeItemRecycleViewAdapter.OnSelectTimeInteractionListener {

    private CarParkingViewModel data;

    private com.haibin.calendarview.CalendarView mCalendarView;
    private TextView mTitleYearMonth;
    private RecyclerView mListViewHours;
    private RecyclerView mListViewMins;

    private TimeItemRecycleViewAdapter mAdapterHours;
    private TimeItemRecycleViewAdapter mAdapterMins;

    private String onlyDate="";
    private String onlyTime="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PopEditCarParkingBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_edit_car_parking);

        data = (CarParkingViewModel) getIntent().getSerializableExtra(LbPop.LbKey.CAR_PARKING);

        String[] timeDate = data.timeIn.split(" ");
        onlyTime = timeDate[0];
        onlyDate = timeDate[1];

        //binding
        binding.setCarParkingViewModel(data);
        binding.setHandler(new CarParkingEvent(this));
        mCalendarView = binding.customCalendarView;
        mTitleYearMonth = binding.titleYearMonth;
        mListViewHours = binding.listViewHours;
        mListViewMins = binding.listViewMins;

        Calendar mDesireTime = java.util.Calendar.getInstance();
        setupCalendarView(mDesireTime);
        setupListTimeView(mDesireTime);
    }

    private void setupCalendarView(Calendar mDesireTime) {
        // set estimated complete time to calendar
        try {
            long date = Conversion.string_2_date_long(onlyDate);

            if (date > 0) {
                mDesireTime.setTimeInMillis(date);
            }

            mCalendarView.scrollToCalendar(mDesireTime.get(java.util.Calendar.YEAR), mDesireTime.get(java.util.Calendar.MONTH) + 1, mDesireTime.get(java.util.Calendar.DATE));

            mCalendarView.setOnCalendarSelectListener(this);
            mTitleYearMonth.setText(SimpleWeekView.getMonthName(mDesireTime.get(java.util.Calendar.MONTH) + 1) + " " + mDesireTime.get(java.util.Calendar.YEAR));

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setupListTimeView(Calendar mDesireDate) {
        // timetable
        if (!TextUtils.isEmpty(onlyTime)) {
            String time[] = onlyTime.split(":");
            mDesireDate.set(java.util.Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
            mDesireDate.set(java.util.Calendar.MINUTE, Integer.valueOf(time[1]));
            mDesireDate.set(java.util.Calendar.SECOND, 0);
        }

        //hour view
        ArrayList<String> mItemsHours = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.hour_table)));
        mAdapterHours = new TimeItemRecycleViewAdapter(this, mItemsHours, this,0);
        mListViewHours.setAdapter(mAdapterHours);

        ArrayList<String> mItemsMins = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.min_table)));
        mAdapterMins = new TimeItemRecycleViewAdapter(this, mItemsMins, this,1);
        mListViewMins.setAdapter(mAdapterMins);

        setTimeTable(mDesireDate);
    }

    @Override
    public void onCalendarOutOfRange(com.haibin.calendarview.Calendar calendar) {

    }

    @Override
    public void onCalendarSelect(com.haibin.calendarview.Calendar calendar, boolean isClick) {
        mTitleYearMonth.setText(SimpleWeekView.getMonthName(calendar.getMonth()) + " " + calendar.getYear());

        if (isClick) {
            onlyDate = Conversion.date_2_string(calendar.getTimeInMillis());
            data.setTimeIn(onlyTime + " " + onlyDate);
        }
    }

    @Override
    public void onSelectHourInteraction(String item) {
        if (!TextUtils.isEmpty(item)) {
            onlyTime = item + ":" + onlyTime.split(":")[1];
            data.setTimeIn(onlyTime + " " + onlyDate);
        }
    }

    @Override
    public void onSelectMinInteraction(String item) {
        if (!TextUtils.isEmpty(item)) {
            onlyTime = onlyTime.split(":")[0] + ":" + item;
            data.setTimeIn(onlyTime + " " + onlyDate);
        }
    }


    private void setTimeTable(java.util.Calendar dateTime) {
        int posHour = dateTime.get(Calendar.HOUR_OF_DAY);
        mAdapterHours.setSelectTime(posHour);
        mListViewHours.scrollToPosition(posHour);

        int posMin = dateTime.get(Calendar.MINUTE);
        mAdapterMins.setSelectTime(posMin);
        mListViewMins.scrollToPosition(posMin);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
