package com.next.smartparkingticket.pop_up;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.next.smartparkingticket.R;
import com.next.smartparkingticket.databinding.PopCarInDetailBinding;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.lib.Sharing;
import com.next.smartparkingticket.lib.car_in.CarIn;
import com.next.smartparkingticket.lib.car_in.CarInEvent;
import com.next.smartparkingticket.lib.car_in.CarInViewModel;
import com.next.smartparkingticket.lib.helper.Devicer;
import com.next.smartparkingticket.lib.helper.Getter;
import com.next.smartparkingticket.lib.ticket.Ticket;
import com.next.smartparkingticket.lib.ticket.TicketEvent;

import java.io.File;

public class PopCarInDetailActivity extends AppCompatActivity {

    private CarIn data;
    private Ticket ticket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PopCarInDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_car_in_detail);

        //binding
        data = (CarIn) getIntent().getSerializableExtra(LbPop.LbKey.CAR_IN);
        binding.setCarIn(data);
        binding.setCarInViewModel(new CarInViewModel(data));

        createTicket();
        binding.setTicket(ticket);

        binding.setTicketHandler(new TicketEvent(this));
        binding.setCarInHandler(new CarInEvent(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case LbPop.LbCode.CAMERA_REQUEST:
                handleActivityResultCamera(resultCode);
                Devicer.photoFile = null;
                break;
        }
    }

    private void handleActivityResultCamera(int resultCode) {
        if (resultCode == RESULT_CANCELED) {
            if (Devicer.photoFile != null && Devicer.photoFile.exists()) {
                Devicer.photoFile.delete();
            }
        } else if (resultCode == RESULT_OK) {
            //remove current file if existing
            if(!ticket.getImgUrl().isEmpty()){
                File removedFile = new File(ticket.getImgUrl());
                removedFile.delete();
            }

            ticket.setImgUrl(Getter.getRealPathFromURI(this, Uri.fromFile(Devicer.photoFile)));
        }
    }

    private void createTicket(){
        if(!data.getCarCode().isEmpty()){
            //get used ticket
            for(Ticket usedTicket:Sharing.Instance().getUsedTickets()){
                if(usedTicket.getCode().equals(data.getCarCode())){
                    ticket = usedTicket;
                    return;
                }
            }
        }else {
            ticket = Sharing.Instance().getTicket();
        }
    }
}
