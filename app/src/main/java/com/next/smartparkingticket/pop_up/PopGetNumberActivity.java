package com.next.smartparkingticket.pop_up;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.next.smartparkingticket.R;
import com.next.smartparkingticket.databinding.PopGetNumberBinding;
import com.next.smartparkingticket.labels.LbPop;
import com.next.smartparkingticket.lib.Car;
import com.next.smartparkingticket.lib.car_in.CarInEvent;
import com.next.smartparkingticket.lib.helper.Checker;
import com.next.smartparkingticket.lib.helper.Devicer;

import java.io.IOException;
import java.util.List;

public class PopGetNumberActivity extends AppCompatActivity {

    private ImageButton btnCapture;
    private ImageButton ibtnClose;
    private EditText etCarNumber;

    private Car car = new Car();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PopGetNumberBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_get_number);
        binding.setCar(car);
        binding.setHandler(new CarInEvent(this));
        binding();

        setupPopup();
        setupButtons();
    }

    private void setupPopup() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .9), (int) (height * .5));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = 0;
        getWindow().setAttributes(params);
    }

    private void binding() {
        etCarNumber = findViewById(R.id.etCarNumber);
        btnCapture = findViewById(R.id.btnCapture);
        ibtnClose = findViewById(R.id.ibtnClose);
    }

    private void setupButtons() {
        btnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open camera to capture
                Devicer.openCamera(v.getContext(),"car_number");
            }
        });

        ibtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case LbPop.LbCode.CAMERA_REQUEST:
                handleActivityResultCamera(resultCode);
                Devicer.photoFile = null;
                break;
        }
    }

    private void handleActivityResultCamera(int resultCode) {
        if (resultCode == RESULT_CANCELED) {
            if (Devicer.photoFile != null && Devicer.photoFile.exists()) {
                Devicer.photoFile.delete();
            }
        } else if (resultCode == RESULT_OK) {
            getTextFromImage(this, Uri.fromFile(Devicer.photoFile));
        }
    }

    private void getTextFromImage(Context context, Uri uri) {
        try {
            FirebaseVisionImage image = FirebaseVisionImage.fromFilePath(context, uri);
            FirebaseVisionTextRecognizer detector = FirebaseVision.getInstance().getOnDeviceTextRecognizer();
            detector.processImage(image).addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                @Override
                public void onSuccess(FirebaseVisionText firebaseVisionText) {
                    List<FirebaseVisionText.TextBlock> blockList = firebaseVisionText.getTextBlocks();
                    if (blockList.size() == 0) {
                        Toast.makeText(context, "Không có ký tự nào được tìm thấy trong hình", Toast.LENGTH_SHORT).show();
                    } else {
                        String detectText = parse(blockList);
                        etCarNumber.setText(detectText);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(context, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("onFailure: ", e.getMessage());
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String parse(List<FirebaseVisionText.TextBlock> blockList){
        String detectText = "";

        for (FirebaseVisionText.TextBlock block : blockList) {
            for (FirebaseVisionText.Line line:block.getLines()){
                for (FirebaseVisionText.Element element:line.getElements()){
                    detectText += element.getText();
                }

                if(block.getLines().indexOf(line)< block.getLines().size()-1){
                    detectText += "\n";
                }
            }
        }

        return detectText;
    }
}
