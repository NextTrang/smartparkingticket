package com.next.smartparkingticket;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.next.smartparkingticket.lib.ModelPreferencesManager;
import com.next.smartparkingticket.lib.Sharing;

public class App extends Application implements ContextProvider {

    private Activity currentActivity;

    @Override
    public Context getActivityContext() {
        return currentActivity;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ModelPreferencesManager.Instance().with(this);

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                App.this.currentActivity = activity;
            }

            @Override
            public void onActivityStarted(Activity activity) {
                App.this.currentActivity = activity;
            }

            @Override
            public void onActivityResumed(Activity activity) {
                App.this.currentActivity = activity;

                if(activity.getClass() != MainActivity.class ){
                    //below codes only apply for MainActivity
                    return;
                }

                //Reload data from server
                ((MainActivity)activity).refresh();
            }

            @Override
            public void onActivityPaused(Activity activity) {
                App.this.currentActivity = null;
            }

            @Override
            public void onActivityStopped(Activity activity) {
                // don't clear current activity because activity may get stopped after
                // the new activity is resumed
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                // don't clear current activity because activity may get destroyed after
                // the new activity is resumed
            }
        });
    }
}
